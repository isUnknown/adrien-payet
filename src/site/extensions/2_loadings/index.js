window.onpopstate = function () {
  window.location.href = window.location.href
}
const contentWrapper = document.querySelector("#content")
listenLinks()

function go(event, link) {
  event.preventDefault()
  const url = link.getAttribute("href") + "/"
  const data = link.getAttribute("href") + "/data.json"

  if (url === window.location.href) return

  fetch(data)
    .then((res) => res.json())
    .then((data) => {
      history.pushState(null, data.title, url)
      contentWrapper.innerHTML = data.html
      listenLinks()
    })
}

function listenLinks() {
  const links = document.querySelectorAll("a:not([target])")
  links.forEach((link) => {
    link.addEventListener("click", function (event) {
      if (document.querySelector(`script.loadings`).dataset.isActive === "true")
        go(event, link)
    })
  })
}
