increaseTotalWeight = (amount) => {
  if ((document.querySelector("script.animations").dataset.isActive = true)) {
    let previousWeight = parseFloat(totalWeightLength.textContent)
    if (totalWeightUnit.textContent === "ko")
      previousWeight = previousWeight * 1000
    const newWeight = previousWeight + amount

    const step = 100

    function updateWeight(transitionWeight) {
      const weightNode = document.querySelector("#total-weight-length")
      if (transitionWeight < newWeight) {
        transitionWeight += step
        totalWeightLength.textContent = formatWeight(transitionWeight).weight
        totalWeightUnit.textContent = formatWeight(transitionWeight).unit
        requestAnimationFrame(() => updateWeight(transitionWeight))
      }
    }

    updateWeight(previousWeight)
  } else {
    let previousWeight = parseFloat(totalWeightLength.textContent)
    if (totalWeightUnit.textContent === "ko")
      previousWeight = previousWeight * 1000
    const newWeight = previousWeight + amount

    totalWeightLength.textContent = formatWeight(newWeight).weight
    totalWeightUnit.textContent = formatWeight(newWeight).unit
  }
}
