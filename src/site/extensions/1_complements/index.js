const complements = document.querySelectorAll(".more")

complements.forEach((complement) => {
  const box = document.createElement("div")
  box.classList.add("complement")

  const text = complement.dataset.complement
  box.innerHTML = `<p>${text}</p>`

  complement.addEventListener("mouseenter", () => {
    showComplement(complement, box)
  })
  complement.addEventListener("mouseleave", () => {
    hideComplement(complement, box)
  })
})

function showComplement(complement, box) {
  if (
    document.querySelector(`script.complements`).dataset.isActive === "true"
  ) {
    complement.appendChild(box)
  }
}
function hideComplement(complement, box) {
  console.log(document.querySelector("script.complements"))
  if (
    document.querySelector(`script.complements`).dataset.isActive === "true"
  ) {
    complement.removeChild(box)
  }
}
