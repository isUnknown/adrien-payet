## Mentions légales

### Édition, design & développement

Design et code réalisé par moi-même :  
Adrien Payet, auto-entrepreneur et membre du studio Praticable.
BAM
SCOP SAS à capital variable
Siège social : 121 rue Haxo 75019 Paris
Email : [contact@praticable.fr](mailto:contact@praticable.fr)  
SIRET : 814 634 135 000 28
Code APE : 7410Z (Activités spécialisées de design)

### Hébergement

Sitre web hébergé par OVH  
SAS au capital de 10 174 560 €  
RCS Lille Métropole 424 761 419 00045  
Code APE 2620Z  
N° TVA : FR 22 424 761 419  
Siège social : 2 rue Kellermann - 59100 Roubaix - France  
OVH SAS est une filiale de la société OVH Groupe SA, société immatriculée au RCS de Lille sous le numéro 537 407 926 sise 2, rue Kellermann, 59100 Roubaix.
Site web : https://www.ovhcloud.com

### Contenus

L'ensemble des contenus de ce site (textes et media) sont mis à disposition selon les termes de la license [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

### Outils et crédits

- Ce site a été codé en HTML, CSS, Javascript et PHP à l'aide de l'éditeur de texte [VS Code](https://code.visualstudio.com/), assisté par [ChatGPT](https://chat.openai.com/) (modèle GPT-4). Le code source est disponible sur mon [GitLab](https://framagit.org/isUnknown/adrien-payet).
- La polices utilisées (lorsque l'extension est active) est la [Switzer](https://www.fontshare.com/fonts/switzer) réalisée par [Indian type foundry](https://www.indiantypefoundry.com/).
- Le site repose sur un générateur de sites statiques extensible original, réalisé à l'occasion. Baptisé "extensions", il permet de réaliser des sites ultra léger extensible. Pour structurer les données, il utilise les formats .yml, .md et .json.

### Confidentialité

Ce site n'utilise aucun système d'enregistrement des navigations des visiteurs.
