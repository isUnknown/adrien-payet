pense, écrit, enseigne, code

## les formes et les techniques

simples, sobres, soignées, praticables  
et poétiquement travaillées

développe des interfaces web  
parle design

travaille les phrases, les formes, les codes  
avec des designers, architectes, artistes  
toujours avec le [studio Praticable](https://praticable.fr)  
de temps avec [User Studio](https://user.io/), [Nicolas Delaroche](https://nicolasdelaroche.com/)  
c'est arrivé avec [&Givry](https://andgivry.com/), [Le Bureau des Usages](https://rfstudio.fr/bu/), [bdcconseil](https://bdcconseil.fr/)…

aime la philosophie de [Pierre-Damien Huyghe](http://pierredamienhuyghe.fr/1.html)  
parce qu'il dit quelque part que

"l'humain est ce vivant dont toute la vie n'est pas impérativement réglée"

et autres paroles  
spirituelles
