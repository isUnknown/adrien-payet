## Allures anonymes

Ici sont présentées des formes, des choses.
Des toiles de maître et des chiffons ménagers, des sculptures souveraines
et des balais quelconques, des portes et des peintures, des entrelacs de
poutres, des installations électriques. Au cœur des foyers, dans
d'improbables cohabitations, on voit ainsi parfois se brouiller
joyeusement des classes d'objets habituellement hiérarchisées. Dans les
lieux de vie des collectionneuses et collectionneurs privés voisinent le
rare et l'ordinaire.

Marchandises qualifiées et classées, les choses et les œuvres, du reste,
le demeurent là-bas, hors-champ, au-delà des cadres.

Mais ici au sein des photographies, voici les choses allégées du poids de
leurs valeurs mondaines. Découvertes comme formes parmi d'autres, elles
n'ont plus d'autres noms.

_Absents… les titres des œuvres. Absentes, les signatures des auteurs.
Absents, les noms des propriétaires. Absentes, toutes les personnes.
Présentes, en revanche, les allures anonymes._
