<?php
use League\CommonMark\MarkdownConverter;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;

class Utility {  
  public static function getMarkdownParser() {
        // Initialize Markdown environment with extensions.
    $mdConf = [
      'external_link' => [
          'internal_hosts' => 'http://localhost:8888', // Don't forget to set this!
          'open_in_new_window' => true,
          'html_class' => 'external-link',
      ],
    ];

    $mdEnvironment = new Environment($mdConf);
    $mdEnvironment->addExtension(new CommonMarkCoreExtension());
    $mdEnvironment->addExtension(new ExternalLinkExtension);

    return $mdParser = new MarkdownConverter($mdEnvironment);
  }

  public static function formatWeight($bytes) {
    if ($bytes >= 1_000_000_000) {
        $bytes = number_format($bytes / 1_000_000_000, 2) . ' Go';
    } elseif ($bytes >= 1_000_000) {
        $bytes = number_format($bytes / 1_000_000, 1) . ' Mo';
    } elseif ($bytes >= 1_000) {
        $bytes = number_format($bytes / 1_000, 0) . ' Ko';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' octets';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' octet';
    } else {
        $bytes = '0 octets';
    }

    return $bytes;
  }

  public static function copyDirectory($source, $dest) {
    if (is_dir($source)) {
        if (!is_dir($dest)) {
            mkdir($dest, 0777, true);
        }
        $files = scandir($source);
        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                self::copyDirectory("$source/$file", "$dest/$file");
            }
        }
    } elseif (file_exists($source)) {
        copy($source, $dest);
    }
  }

  public static function removeDir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object)) {
                    self::removeDir($dir . "/" . $object);
                } else {
                    unlink($dir . "/" . $object);
                }
            }
        }
        rmdir($dir);
    }
  }

  public static function initDir($dir) {
    try {
      self::removeDir($dir);
      mkdir($dir);
    } catch (\Throwable $th) {
      echo $th;
    }
  }
}