<?php

/**
 * Most common paths.
 */
class Paths {
  /** 
     * Css path to get all syles.
     * 
     * @var String
     */ 
    public const CSS_PATH = __DIR__ . '/../site/assets/css/style.css';

    /**
     * Dist path for builds.
     * 
     * @var String
     */
    public const DIST_PATH = __DIR__ . '/../../dist';
}