<?php
require_once __DIR__ . '/WebsiteBuilder.php';
require_once __DIR__ . '/Utility.php';

$websiteBuilder = new WebsiteBuilder();

$extensionsDirs = glob(__DIR__ . '/../site/extensions/*/');
$listsDirs = glob(__DIR__ . '/../site/content/lists/*/');
$pagesDirs = glob(__DIR__ . '/../site/content/pages/*/');
$templatesDir = glob(__DIR__ . '/../site/templates/*.html');
$homePageDir = __DIR__ . '/../site/content/';

$websiteBuilder->buildHeader($listsDirs, $extensionsDirs, $templatesDir);

$websiteBuilder->buildHomePage();

foreach ($pagesDirs as $pageDir) {
  $page = $websiteBuilder->buildPageData($pageDir);
  $websiteBuilder->buildHtmlPage($page);
}


$pageFiles = glob(Paths::DIST_PATH . '/pages/*/*.html');
$homePageFile = array(Paths::DIST_PATH . '/index.html');
$templateFiles = glob(Paths::DIST_PATH . '/templates/*.html');
$htmlFiles = array_merge($pageFiles, $homePageFile, $templateFiles);
$websiteBuilder->addFileSizeToLinks($htmlFiles);