<?php
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/Paths.php';
require_once __DIR__ . '/Utility.php';

/**
 * Build processes based on DataGenerator and Renderer classes.
 */
class WebsiteBuilder {    

    /**
     * Markdown converter for converting Markdown to HTML.
     *
     * @var MarkdownConverter
    */
    private $mdParser;

    /** 
     * Data generator to generate data from YAML and JSON files.
     * 
     * @var DataGenerator
     */
    private $dataGenerator;
    
    /** 
     * Html generator to generate HTML from Data.
     * 
     * @var Renderer
     */
    private $Renderer;

    /**
     * Constructor for the WebsiteBuilder class.
     */
    public function __construct() {
        $this->mdParser = Utility::getMarkdownParser();

        $this->dataGenerator = new DataGenerator();
        $this->Renderer = new Renderer();
    }

  /**
   * Build lists and extensions in HTML template.
   *
   * @param Array $listsDirs
   * @param Array $extensionsDirs
   * @param Array $templatesDir
   * @return type
   **/
  public function buildHeader($listsDirs, $extensionsDirs, $templatesDir) {
    $lists = [];
    $extensions = [];

    foreach ($listsDirs as $listDir) {
      $listData = self::buildListData($listDir);
      $lists[] = $listData;
    }

    Utility::initDir(Paths::DIST_PATH . '/extensions');
    foreach ($extensionsDirs as $extensionPath) {
      $extensions[] = self::buildExtensionData($extensionPath);
    }

    Utility::initDir(Paths::DIST_PATH . '/templates');
    foreach ($templatesDir as $templatePath) {
      self::buildHtmlExtensions($extensions, $templatePath);
      self::buildHtmlLists($lists, $templatePath);
    }

    echo "Basic template builded with extensions and lists.\n";
  }

  /**
   * Build the home page by integrating data from JSON and templates.
   */
  public function buildHomePage() {
      $homeData = $this->dataGenerator->buildHomeData();
      $this->Renderer->buildHtmlHomePage($homeData);
  }

  /**
   * Delegated method for buildListData from DataGenerator.
   * See DataGenerator::buildListData for more details
   *
   * @param string $listDir The directory path.
   * @return array The data list.
   **/
  public function buildListData($listDir) {
    return $this->dataGenerator->buildListData($listDir);
  }

  /**
     * Build extension data from extension files (CSS, JS, etc.).
     * See DataGenerator::buildExtensionData for more details
     * 
     * @param string $extensionPath Path to the extension directory.
     * @return array|null Array of extension data or null if no valid data found.
     */
    public function buildExtensionData($extensionPath) {
      return $this->dataGenerator->buildExtensionData($extensionPath);
  }

  /**
   * Build page data from Markdown and YAML files and generate a data JSON file.
   *
   * @param string $pageDirPath Path to the page directory.
   * @param bool $isHome Indicates whether it's the home page.
   * @return array|null Array of page data or null if no valid data found.
  */
  public function buildPageData($pageDirPath, $isHome = false) {
    return $this->dataGenerator->buildPageData($pageDirPath, $isHome = false);
  }

  /**
     * Generate an HTML page from page data and save it to a file.
     *
     * @param array $pageData Array containing page data.
     */
    public function buildHtmlPage($pageData) {
      $this->Renderer->buildHtmlPage($pageData);
  }

  /**
   * Build HTML content for extensions and insert it into a template.
   *
   * @param array $items Array of extension data.
   * @param string $templatePath Path to the HTML template.
   */
  public function buildHtmlExtensions($items, $templatePath) {
      $this->Renderer->buildHtmlExtensions($items, $templatePath);
  }

  /**
   * Build HTML content for lists and insert it into a template.
   *
   * @param array $listsData Array of list data.
   * @param string $templatePath Path to the HTML template.
   */
  public function buildHtmlLists($listsData, $templatePath) {
      $this->Renderer->buildHtmlLists($listsData, $templatePath);
  }


  /**
   * Modify each internal links to add its targeted file size.
   *
   * @param Array $htmlFiles Array of files paths to process.
   **/
  public function addFileSizeToLinks($htmlFiles) {
    foreach ($htmlFiles as $filePath) {
        $htmlContent = file_get_contents($filePath);

        $dom = new DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($htmlContent, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $aTags = $dom->getElementsByTagName('a');
        $contentTag = $dom->getElementById('content');
        
        if ($contentTag) {
            $contentHtml = $dom->saveHTML($contentTag);
            $contentSize = strlen($contentHtml);
        }

        foreach ($aTags as $aTag) {
            if (str_contains($aTag->getAttribute('class'), 'no-size')) return;
            $href = $aTag->getAttribute('href');

            if (!str_starts_with($href, 'http')) {
              $linkedFilePath = realpath(Paths::DIST_PATH . $href . '/index.html');

              if (file_exists($linkedFilePath)) {
                  $fullPagesize = filesize($linkedFilePath);
                  $linkText = $aTag->nodeValue . " [" . Utility::formatWeight($fullPagesize) . "]";
                  $aTag->nodeValue = $linkText;
                  $aTag->setAttribute('data-page-size', $fullPagesize);
                  $aTag->setAttribute('data-content-size', $contentSize);
              }
            }
        }

        $newHtmlContent = $dom->saveHTML();
        $newHtmlContent = html_entity_decode($newHtmlContent);
        file_put_contents($filePath, $newHtmlContent);
    }
  }
}
