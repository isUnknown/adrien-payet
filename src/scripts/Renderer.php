<?php
/**
 * Build HTML files from data generated through DataGenerator class.
 */
class Renderer {
  /**
   * Consolidated CSS from CSS files.
   *
   * @var string
   */
  private $css;

  /**
   * Markdown converter for converting Markdown to HTML.
   *
   * @var MarkdownConverter
  */
  private $mdParser;

  /**
   * Generate an HTML page from page data and save it to a file.
   *
   * @param array $pageData Array containing page data.
   */
  public function buildHtmlPage($pageData) {
    try {
      $html = isset($pageData['props']['template']) ? file_get_contents(Paths::DIST_PATH . '/templates/' . $pageData['props']['template'] . '.html') : file_get_contents(Paths::DIST_PATH . '/templates/default.html');
    } catch (Exception $e) {
      throw $e;
    }
    if (!file_exists($pageData['dirPath'])) {
        mkdir($pageData['dirPath'], 0777, true);
    }

    $newHtml = preg_replace('/<section id="content">([\s\S]*?)<\/section>/', '<section id="content">' . $pageData['html'] . '</section>', $html);

    $newHtml = $this->injectCss($newHtml);
    $htmlFile = fopen($pageData['dirPath'] . '/index.html', 'w');
    fwrite($htmlFile, $newHtml);
    fclose($htmlFile);
  }

  /**
   * Build HTML content for extensions and insert it into a template.
   *
   * @param array $items Array of extension data.
   * @param string $templatePath Path to the HTML template.
  */
  public function buildHtmlExtensions($items, $templatePath) {
    try {
      $html = file_get_contents($templatePath);
    } catch (Exception $e) {
      throw $e;
    };
    
    if (strpos($html, 'id="extensions"') !== false) {
        $lis = '';

        foreach ($items as $item) {
            $props = $item['props'];
            $title = isset($props['title']) ? $props['title'] : 'Sans titre';
            $description = isset($props['description']) ? '<p class="details">' . $props['description'] . '</p>' : '';
            $titleAttribute = isset($props['description']) ? 'title="' . $props['description'] . '"' : '';

            $lis .= <<<HTML
                <li id="extension-{$item['dirName']}">
                    <button onclick="toggleExtension(event, '{$item['dirName']}')"{$titleAttribute} data-weight="{$item['rawWeight']}">
                        {$title} <span class="extension-weight">[+{$item['formattedWeight']}]</span>
                        $description
                    </button>
                </li>
            HTML;

        }
        
        $templateBuildPath = str_replace('../site/templates', '../../dist/templates', $templatePath);
        $newHtml = preg_replace('/<ul id="extensions"><\/ul>/', '<ul id="extensions">' . $lis . '</ul>', $html);
        try {
            $templateFile = fopen($templateBuildPath, 'w');
            fwrite($templateFile, $newHtml);
            fclose($templateFile);
        } catch (Exception $e) {
            throw $e;
        };
        echo "Extensions have been successfully inserted into the HTML file.\n";
    } else {
        echo "The <ul id=\"extensions\"> tag was not found.";
    }
  }

  /**
   * Build HTML content for lists and insert it into a template.
   *
   * @param array $listsData Array of list data.
   * @param string $templatePath Path to the HTML template.
  */
  public function buildHtmlLists($listsData, $templatePath) {    
    $templateBuildPath = str_replace('../site/templates', '../../dist/templates', $templatePath);
    try {
      $HTML = file_get_contents($templateBuildPath);
    } catch (Exception $e) {
      throw $e;
    };
    $mainContent = '';

    foreach ($listsData as $listData) {
        $props = $listData['props'];
        $sectionTitle = isset($props['title']) ? $props['title'] : 'Untitled';

        $sectionHtml = <<<HTML
            <section class="column">
                <header class="section-header">
                    <button class="toggle-column" onclick="toggleColumn(event)">
                        <h2>{$sectionTitle}&nbsp;&nbsp;</h2>
                        <hr>
                    </button>
                </header>
                <ul id="{$listData['dirName']}-list" class="list"></ul>
            </section>
        HTML;
        
        $listsListHTML = '';

        foreach ($listData['items'] as $item) {
            $title = isset($item['props']['title']) ? $item['props']['title'] : 'Untitled';
            $link = isset($item['props']['link']) ? $item['props']['link'] : '#';
            $description = isset($item['props']['description']) ? $this->mdParser->convert($item['props']['description']) : '';

            $titleHtml = '';

            if (isset($item['props']['link'])) {
                if (str_starts_with($item['props']['link'], '/pages')) {
                  $titleHtml = "<a href=\"{$item['props']['link']}\">$title</a>";
                } else {
                  $titleHtml = "<a target=\"_blank\" href=\"$link\">$title</a>";
                }
            } else {
                $titleHtml = "<p>$title</p>";
            }
            $descriptionHtml = isset($item['props']['description']) ? 
                "<div class=\"details\">$description</div>" 
                : '';

            $listsListHTML .= <<<HTML
                <li>
                    $titleHtml
                    $descriptionHtml
                </li>
            HTML;
        }
        $sectionPattern = '/<ul id="' . preg_quote($listData['dirName']) . '-list" class="list">/';
        $mainContent .= preg_replace($sectionPattern, '$0' . $listsListHTML, $sectionHtml);
    }
    $newHTML = str_replace('<div id="columns"></div>', '<div id="columns">' . $mainContent . '</div>', $HTML);
    try {
      file_put_contents($templateBuildPath, $newHTML, LOCK_EX);
      echo "Lists have been successfully inserted into the HTML file.\n";
    } catch (Exception $e) {
      throw $e;
    };
  }

  // private function getPageWeights($page) {
  //   $templateName = isset($page['props']['template']) ? $page['props']['template'] : 'default';
  //   $template = file_get_contents(__DIR__ . '/../../dist/templates/' . $templateName . '.html');
  //   $template = $this->injectCss($template);
  //   $templateWeight = strlen($template);
    
  //   $contentWeightRaw = strlen($page['html']);
  //   $contentWeightFormatted = Utility::formatWeight($contentWeightRaw);

  //   $fullPageWeightRaw = $templateWeight + $contentWeightRaw;
  //   $fullPageWeightFormatted = Utility::formatWeight($fullPageWeightRaw);

  //   return [
  //     'content' => [
  //       'raw' => $contentWeightRaw,
  //       'formatted' => $contentWeightFormatted
  //     ],
  //     'fullPage' => [
  //       'raw' => $fullPageWeightRaw,
  //       'formatted' => $fullPageWeightFormatted
  //     ]
  //   ];
  // }

  /**
   * Build the home page using the provided data and templates.
   *
   * @param object $homeData The data for the home page.
  */
  public function buildHtmlHomePage($homeData) {
    $homeTemplate = null;

    if (file_exists(Paths::DIST_PATH . '/templates/home.html')) {
        try {
            $homeTemplate = file_get_contents(Paths::DIST_PATH . '/templates/home.html');
        } catch (Exception $e) {
            throw $e;
        }
    } else {
        try {
            $homeTemplate = file_get_contents(Paths::DIST_PATH . '/templates/default.html');
        } catch (Exception $e) {
            throw $e;
        }
        
        if ($homeTemplate === false) {
            die("Neither home.html nor default.html exists.");
        }
    }

    $homeHtml = $this->injectCss($homeTemplate);
    $homeHtml = preg_replace('/<section id="content">([\s\S]*?)<\/section>/', '<section id="content">' . $homeData['html'] . '</section>', $homeHtml);

    $homeIndex = fopen(Paths::DIST_PATH . '/index.html', 'w');
    fwrite($homeIndex, $homeHtml);
    fclose($homeIndex);
  }

  /**
   * Inject CSS into HTML code.
   *
   * @param string $html HTML code to inject CSS into.
   * @return string HTML code with CSS injected.
  */
  private function injectCss($html) {
    return str_replace('</title>', '</title><style>' . $this->css . '</style>', $html);
  }

  /**
   * Concatenate CSS from multiple files into a single CSS string.
  */
  private function concatCss() {
    $cssFiles = glob(__DIR__ . '/../site/assets/css/*/*.css');

    foreach ($cssFiles as $file) {
        try {
          $this->css .= file_get_contents($file);
        } catch (Exception $e) {
          throw $e;
        }
    }
  }

  public function __construct() {
    $this->concatCss();
    $this->mdParser = Utility::getMarkdownParser();
  }
}