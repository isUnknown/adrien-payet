<?php
require_once __DIR__ . '/Paths.php';
require_once __DIR__ . '/Utility.php';

use Symfony\Component\Yaml\Yaml;

/**
 * Generate data needed for HTML injection through the Renderer class.
 */
class DataGenerator {
  /**
   * Markdown converter for converting Markdown to HTML.
   *
   * @var MarkdownConverter
  */
  private $mdParser;

  /**
   * Read properties from a YAML file.
   *
   * @param string $propsFile Path to the YAML properties file.
   * @return array|null Array of properties or null if file doesn't exist.
   */
  private function readYamlProperties($propsFile) {
    if (file_exists($propsFile)) {
        $props = Yaml::parseFile($propsFile);
        if (isset($props['description'])) {
          $props['description'] = $this->mdParser->convert($props['description']);
        }
        if ($props !== false && isset($props['title'])) {
            return $props;
        }
    }
    return null;
  }

  /**
     * Build extension data from extension files (CSS, JS, etc.) and store it into dist/extensions/<extension-name>/data.json.
     *
     * @param string $extensionPath Path to the extension directory.
     * @return array|null Array of extension data or null if no valid data found.
  */
  public function buildExtensionData($extensionPath) {
    $propsFile = $extensionPath . 'props.yml';

    $props = $this->readYamlProperties($propsFile);

    if ($props !== null) {
        $content = [
            'dirName' => basename($extensionPath),
            'props' => $props,
            'css' => null,
            'js' => null,
            'files' => null
        ];

        $extensionCodeFiles = scandir($extensionPath);

        $extensionFiles = [];

        foreach ($extensionCodeFiles as $filePath) {
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            if ($fileExtension === 'css' || $fileExtension === 'js') {
                try {
                  $fileContent = file_get_contents($extensionPath . $filePath);
                } catch (Exception $e) {
                  throw $e;
                };
                $content[$fileExtension] = $fileContent;
            } elseif (!str_contains($filePath, '.')) {
              $extensionFiles[] = $extensionPath . '/' . $filePath;
            }
        }
        $buildDirPath = Paths::DIST_PATH . '/extensions/' . basename($extensionPath);
        if (!is_dir($buildDirPath)) {
            mkdir($buildDirPath, 0777, true);
        }
        foreach ($extensionFiles as $extensionFile) {
          Utility::copyDirectory($extensionFile, $buildDirPath . '/' . basename($extensionFile));
        }
        $dataFile = fopen($buildDirPath . '/data.json', 'w');
        
        if ($dataFile) {
            fwrite($dataFile, json_encode($content));
            fclose($dataFile);
            echo "The data.json file for the extension '{$props['title']}' has been successfully updated.\n";
        } else {
            echo "Unable to open the data.json file for the extension '{$props['title']}.\n";
        }

        $totalWeight = isset($props['additionalWeight']) ? $props['additionalWeight'] + filesize($buildDirPath . '/data.json') : filesize($buildDirPath . '/data.json');
        $content['rawWeight'] = $totalWeight;
        $content['formattedWeight'] = Utility::formatWeight($totalWeight);

        return $content;
    }

    return null;
  }

  /**
   * Format a file weight in human-readable units.
   *
   * @param float $weight File weight in bytes.
   * @return string Formatted file weight with units.
  */ 
  private function formatWeight($weight) {
    $units = ['byte', 'KB', 'MB', 'GB'];
    $unitIndex = 0;

    while ($weight >= 1000 && $unitIndex < count($units) - 1) {
        $weight /= 1000;
        $unitIndex++;
    }

    return round($weight, 2) . ' ' . $units[$unitIndex];
}

  /**
   * Build list data from YAML files.
   *
   * @param string $listDirPath Path to the directory containing list data.
   * @return array|null Array of list content or null if no valid data found.
  */
  public function buildListData($listDirPath) {
    $listPropsFile = $listDirPath . 'props.yml';

    $listProps = $this->readYamlProperties($listPropsFile);

    if ($listProps !== null) {
        $content = [
            'dirName' => basename($listDirPath),
            'props' => $listProps,
            'items' => []
       ];

        $listFiles = glob($listDirPath . '*');
        foreach ($listFiles as $listFile) {
            if (str_contains($listFile, 'props')) break;
            $itemProps = Yaml::parseFile($listFile);

            if ($itemProps !== false && isset($itemProps['title'])) {
                $item = [
                  'props' => $itemProps
                ];
                
                if (isset($itemProps['link']) && str_starts_with($itemProps['link'], '/')) {
                  $item['type'] = 'page';
                } 
                $content['items'][] = $item;
            }
        }
        return $content;
    }

    return null;
  }

  /**
   * Build page data from Markdown and YAML files and generate a data JSON file.
   *
   * @param string $pageDirPath Path to the page directory.
   * @param bool $isHome Indicates whether it's the home page.
   * @return array|null Array of page data or null if no valid data found.
  */
  public function buildPageData($pageDirPath, $isHome = false) {      
      $propsFile = $pageDirPath . '/props.yml';
      $markdownFiles = glob($pageDirPath . '/*.md');
      if (!empty($markdownFiles)) {
          $markdownFile = $markdownFiles[0];
      } else {
          echo "No Markdown file found in the directory $pageDirPath.";
      }

      $props = $this->readYamlProperties($propsFile);

      if ($props !== null && file_exists($markdownFile)) {
          $data = [
              'dirName' => basename($pageDirPath),
              'dirPath' => $isHome ? Paths::DIST_PATH . '/pages/' : Paths::DIST_PATH . '/pages/' . preg_replace('/[0-9]_/', '', basename($pageDirPath)),
              'props' => $props,
              'html' => [],
              'url' => '/pages/' . preg_replace('/[0-9]_/', '', basename($pageDirPath)),
          ];

          try {
            $markdown = file_get_contents($markdownFile);
          } catch (Exception $e) {
            throw $e;
          };
          $HTML = $this->mdParser->convert($markdown);
          $data['html'] = strval($HTML);

          Utility::initDir($data['dirPath']);
          $dataFile = fopen($data['dirPath'] . '/data.json', 'w');
          if ($dataFile) {
              fwrite($dataFile, json_encode($data));
              fclose($dataFile);
              echo "The data.json file for the page '{$props['title']}' has been successfully updated.\n";
          } else {
              echo "Unable to open the data.json file for the page '{$props['title']}.\n";
          }

          return $data;
      }

      return null;
  }

  /**
   * Extract and process the data for the home page.
   *
   * @return object The processed data for the home page.
  */
  public function buildHomeData() {
    $homeDirPath =  __DIR__ . '/../site/content/pages';
    $propsFile = $homeDirPath . '/props.yml';
    $markdownFiles = glob($homeDirPath . '/*.md');
    
    if (!empty($markdownFiles)) {
        $markdownFile = $markdownFiles[0];
    } else {
        echo "No Markdown file found in the directory $homeDirPath.";
    }

      $props = $this->readYamlProperties($propsFile);

      if ($props !== null && file_exists($markdownFile)) {
          $data = [
              'dirPath' => Paths::DIST_PATH,
              'props' => $props,
              'html' => '',
              'url' => '/',
              'type' => 'page'
          ];

          try {
            $markdown = file_get_contents($markdownFile);
          } catch (Exception $e) {
            throw $e;
          };

          $HTML = $this->mdParser->convert($markdown);
          $data['html'] = strval($HTML);

          $dataFile = fopen($data['dirPath'] . '/data.json', 'w');
          if ($dataFile) {
              fwrite($dataFile, json_encode($data));
              fclose($dataFile);
              echo "Home data successfully builded.\n";
          } else {
              echo "Unable to open the data.json file for the extension '{$props['title']}.\n";
          }

          return $data;
      }

      return null;
  }

  public function __construct() {
    $this->mdParser = Utility::getMarkdownParser();
  }
}